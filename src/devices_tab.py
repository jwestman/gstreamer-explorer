# devices_tab.py
#
# Copyright 2020 James Westman <james@flyingpimonster.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk, Gst, GLib
from gi.repository import Handy as Hdy

from .device_row import DeviceRow

@Gtk.Template(resource_path='/net/flyingpimonster/GstExplorer/ui/devices-tab.ui')
class DevicesTab(Gtk.ScrolledWindow):
    __gtype_name__ = 'GstExplorerDevicesTab'

    listbox = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.monitor = Gst.DeviceMonitor()
        self.bus = self.monitor.get_bus()
        self.bus.add_watch(GLib.PRIORITY_DEFAULT, self.bus_watch_func, None)
        self.monitor.start()

        for device in self.monitor.get_devices():
            self.add_device_row(device)


    def bus_watch_func(self, bus, message, _1):
        if message.type == Gst.MessageType.DEVICE_ADDED:
            device = message.parse_device_added()
            self.add_device_row(device)

        return GLib.Source.CONTINUE



    def add_device_row(self, device):
        device_row = DeviceRow(device)
        self.listbox.add(device_row)

        def on_removed(device):
            self.listbox.remove(device_row)

        device.connect("removed", on_removed)



# utils.py
#
# Copyright 2020 James Westman <james@flyingpimonster.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import Gst


def get_device_icon(device: Gst.Device):
    if device.has_classes("Video/Source"):
        return "camera-video-symbolic"
    elif device.has_classes("Video/Sink"):
        return "display-symbolic"
    elif device.has_classes("Audio/Source"):
        return "audio-input-microphone-symbolic"
    elif device.has_classes("Audio/Sink"):
        return "audio-speakers-symbolic"


def clear_container(container):
    container.foreach(lambda child: container.remove(child))

def to_string(structure, field):
    if structure.has_field_typed(field, Gst.IntRange):
        return ""
    elif structure.has_field_typed(field, Gst.ValueList):
        return ""
    elif structure.has_field_typed(field, Gst.Bitmask):
        return ""
    elif structure.has_field_typed(field, Gst.Fraction):
        _1, num, den = structure.get_fraction(field)
        return "%d/%d" % (num, den)
    else:
        return str(structure.get_value(field))


# window.py
#
# Copyright 2020 James Westman
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio
from gi.repository import Handy as Hdy

from .devices_tab import DevicesTab
from .elements_tab import ElementsTab
from .plugins_tab import PluginsTab

@Gtk.Template(resource_path='/net/flyingpimonster/GstExplorer/ui/window.ui')
class GstExplorerWindow(Hdy.ApplicationWindow):
    __gtype_name__ = 'GstExplorerWindow'

    stack = Gtk.Template.Child()

    def __init__(self, application):
        super().__init__(application=application)
        self.application = application

        # ACTIONS
        about = Gio.SimpleAction.new("about", None)
        about.connect("activate", self.on_about)
        self.add_action(about)

        # TABS
        plugins = PluginsTab()
        self.stack.add_titled(plugins, "plugins", _("Plugins"))
        self.stack.child_set_property(plugins, "icon-name", "application-x-addon-symbolic")

        elements = ElementsTab()
        self.stack.add_titled(elements, "elements", _("Elements"))
        self.stack.child_set_property(elements, "icon-name", "elements-symbolic")

        devices = DevicesTab()
        self.stack.add_titled(devices, "devices", _("Devices"))
        self.stack.child_set_property(devices, "icon-name", "devices-symbolic")

    def on_about(self, action, param):
        dialog = Gtk.AboutDialog()
        dialog.props.transient_for = self

        dialog.set_license_type(Gtk.License.GPL_3_0)
        dialog.set_version(self.application.version)
        dialog.set_authors(["James Westman <james@flyingpimonster.net>"])

        dialog.run()
        dialog.hide()


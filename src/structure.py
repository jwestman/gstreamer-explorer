# structure.py
#
# Copyright 2020 James Westman <james@flyingpimonster.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import Gtk
from gi.repository import Handy as Hdy

from . import utils

class Structure(Gtk.Grid):
    __gtype_name__ = 'GstExplorerStructure'

    def __init__(self, structure=None, **kwargs):
        super().__init__(**kwargs)

        self._structure = None
        self.props.visible = True
        self.props.column_spacing = 12
        self.props.row_spacing = 6

        if structure is not None:
            self.structure = structure


    @property
    def structure(self):
        return self._structure

    @structure.setter
    def structure(self, new_structure):
        self._structure = new_structure
        utils.clear_container(self)
        
        if new_structure is None:
            null_label = Gtk.Label(_("NULL"))
            self.add(null_label)
            return

        name_label = Gtk.Label(new_structure.get_name())
        name_label.props.halign = Gtk.Align.START
        name_label.props.selectable = True
        self.attach(name_label, 0, 0, 2, 1)

        fields = []
        for n in range(new_structure.n_fields()):
            fields.append(new_structure.nth_field_name(n))
        fields.sort()

        y = 1
        for field in fields:
            key_label = Gtk.Label(field)
            key_label.props.halign = Gtk.Align.END
            key_label.get_style_context().add_class("dim-label")
            key_label.props.selectable = True
            key_label.props.valign = Gtk.Align.START
            self.attach(key_label, 0, y, 1, 1)

            val_label = Gtk.Label(utils.to_string(new_structure, field))
            val_label.props.halign = Gtk.Align.START
            val_label.props.selectable = True
            val_label.props.wrap = True
            val_label.props.xalign = 0
            self.attach(val_label, 1, y, 1, 1)

            y += 1

        self.show_all()


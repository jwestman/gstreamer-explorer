# main.py
#
# Copyright 2020 James Westman
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio, Gst, GLib
from gi.repository import Handy as Hdy

from .window import GstExplorerWindow


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='net.flyingpimonster.GstExplorer',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_startup(self):
        Hdy.init()
        Gtk.Application.do_startup(self)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = GstExplorerWindow(self)
        win.present()


def main(version):
    Gst.init()

    GLib.set_application_name(_("GStreamer Explorer"))

    app = Application()
    app.version = version
    return app.run(sys.argv)

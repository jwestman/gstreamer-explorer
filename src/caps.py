# caps.py
#
# Copyright 2020 James Westman <james@flyingpimonster.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import Gtk
from gi.repository import Handy as Hdy

from . import utils
from .structure import Structure

class Caps(Gtk.Grid):
    __gtype_name__ = 'GstExplorerCaps'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._caps = None
        self.props.visible = True
        self.props.column_spacing = 12
        self.props.row_spacing = 6
        self.props.orientation = Gtk.Orientation.VERTICAL


    @property
    def caps(self):
        return self._caps

    @caps.setter
    def caps(self, new_caps):
        self._caps = new_caps
        utils.clear_container(self)

        for i in range(new_caps.get_size()):
            cap_struct = new_caps.get_structure(i)
            cap_features = new_caps.get_features(i)
            self.attach(Structure(cap_struct), 0, i, 1, 1)

        self.show_all()


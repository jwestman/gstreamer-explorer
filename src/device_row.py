# device_row.py
#
# Copyright 2020 James Westman <james@flyingpimonster.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Handy as Hdy

from . import utils
from .caps import Caps
from .structure import Structure

@Gtk.Template(resource_path='/net/flyingpimonster/GstExplorer/ui/device-row.ui')
class DeviceRow(Hdy.ExpanderRow):
    __gtype_name__ = 'GstExplorerDeviceRow'

    device_props = Gtk.Template.Child()
    device_caps = Gtk.Template.Child()

    def __init__(self, device, **kwargs):
        super().__init__(**kwargs)

        self.device = device
        self.props.visible = True
        self.props.title = device.props.display_name
        self.props.subtitle = device.get_device_class()

        self.props.icon_name = utils.get_device_icon(self.device)

        self.device_props.structure = self.device.get_properties()
        self.device_caps.caps = self.device.get_caps()


# element_row.py
#
# Copyright 2020 James Westman <james@flyingpimonster.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import Gtk
from gi.repository import Handy as Hdy


class ElementRow(Hdy.ActionRow):
    __gtype_name__ = 'GstExplorerElementRow'

    def __init__(self, element, **kwargs):
        super().__init__(**kwargs)

        self.element = element
        self.props.visible = True
        self.props.title = element.props.name
        self.props.subtitle = element.get_metadata("long-name")

